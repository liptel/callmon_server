#!/bin/bash

# This script automates some required steps for the configuration of the
# callmon server. It is not a complete setup, though. Please refer to the
# user guide for complete installation and configuration proceedure.

# Check if user callmon-probe exists, if he exists skip, otherwise create him
# 0 - user exists, 1 - user does not exist
user="callmon-probe"

user_exists=$(id -u ${user} > /dev/null 2>&1; echo $?)

if [ "$user_exists" == "1" ]; then
    echo "Creating user: $user"
    useradd -m $user
    passwd $user
else
    echo "User $user already exists, therefore skipping."
fi

echo "Checking if there is .ssh and .ssh/authorized_keys in users home directory"
home=$(eval echo ~$user)
if [ ! -d "$home/.ssh" ]; then
    echo ".ssh directory not present in users home. Creating it."
    mkdir "$home/.ssh"
    chown $user:$user "$home/.ssh"
else
    echo "Directory .ssh already exists in users home, skipping"
fi
if [ ! -f "$home/.ssh/authorized_keys" ]; then
    echo "authorized_keys file not present in users home/.ssh/. Creating it."
    touch "$home/.ssh/authorized_keys"
    chown $user:$user "$home/.ssh/authorized_keys"
    chmod 644 "$home/.ssh/authorized_keys"
else
    echo "File .ssh/authorized_keys already exists in users home, skipping"
fi

echo "Changing ownership of the parent directory"
base_folder=$(echo $PWD | sed 's/\/shell//')
chown -R $user:$user $base_folder

# Trying to update MySQL to incorporate cron_tasks table
echo "Updating MySQL"
read -e -p "Please enter MySQL user for callmon server/probe functions. [zabbix]" mysql_name
read -e -p "Please enter MySQL database for callmon server/probe functions. [zabbix]" mysql_db
mysql -u "${mysql_name:-zabbix}" -p "${mysql_db:-zabbix}" < ../sql/create-cron-tasks-table.sql


# Creating link for callmons executable and setting the execution rights
echo "Linking callmons executable"
ln -s $base_folder/python/callmons /usr/bin/callmons

# Inserting Cron tasks
echo "Creating cron entries"
echo "*/5 * * * * callmons -d" > crontable
crontab -u $user crontable

echo "Callmon Server initial configuration done. Exiting."
exit 0
