class Config(object):
    host                = '127.0.0.1'
    port                = 10051
    host_name           = 'VoipHost'
    sender_file         = 'logger.py'
    base_dir            = '/opt/callmon_server/python/'
    system_user         = 'root'
    wav_path            = '/opt/callmon_server/data_wavs/'
    original_wav_file   = '/opt/callmon_server/wav/test_ulaw_25s.wav'
    zabbix_login        = 'Admin'  #zabbix server auths
    zabbix_password     = '4K),@A=VjTtK;S/Ti!,]'
    zabbix_url          = 'http://%s/zabbix'                % host

    tree_base           = '/opt/callmon_server'                             # root of the callmons installation
    base_dir            = '%s/python'                       % tree_base     # python scripts location where the main logic resides
    qtest               = '%s/pesq/pesq'                    % tree_base     # where the testing binary/script is located. IMPORTANT! On x64 Ubuntu/Debian, there is a precompiled version, in other cases please compile it from P.862 sources.
    reference_wav       = '%s/wav/test_ulaw_25s.wav'        % tree_base     # reference wav for pesq comparison. IMPORTANT! It has to be the same on both server and probe
    wav_path            = '%s/data_wavs/'                   % tree_base     # wavs received from the probes waiting for the processing


    map_name            = 'topo_new'
    map_size            = (1280, 800)
    log_file            = '%s/logs/callmons.log'            % tree_base
    map_id_counter      = '%s/map-id/map-id-counter'        % tree_base     # required for any external service that reads the zabbix map, therefore it has to be readable by the web server.

    mysql_host          = host                                              # can point to any IP, but defaults to localhost
    mysql_user          = 'zabbix'
    mysql_pass          = 'XW241vrMmI23I0x77zEU'
    mysql_db            = 'zabbix'

