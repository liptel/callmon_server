#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
from pprint import pprint
import networkx as nx
from zabbix_api import ZabbixAPI
import pygeoip
import itertools

#Class for prepare nodes and edges for map topology from real probes on zabbix. Every probe should be positioned by its IP adress GPS coordinates
class ZabbixMap(object):
    def __init__(self, width, height, mapname ):
        self.edges          = {}
        self.edges_labeled  = {}
        self.pos            = {}
        self.poslist        = {}
        self.maxpos         = 0
        self.probes         = {}
        self.geodata        = {}
        self.limits         = {}
        self.multiplierX    = 1
        self.multiplierY    = 1
        self.padding        = 0.9
        self.x_check        = []                                                                            # array of x coordinates, that was succesfully checked, 2 probes can't have same x and y coordinates
        self.y_check        = {}                                                                            # array of y coordinates, that was successfully checked
        self.width          = width                                                                         # Canvas width
        self.height         = height                                                                        # Canvas height
        self.map_name       = mapname                                                                       # Map name
        self.Grph           = nx.Graph()                                                                    # Temp graph
        self.Cfg            = Config()
        self.Zapi           = ZabbixAPI(server=self.Cfg.zabbix_url, path="", log_level=0)                   # Connection to zabbix server for get probes datas
        #self.Geo            = pygeoip.GeoIP('/home/python/geoip/GeoLiteCity.dat', pygeoip.MEMORY_CACHE)    # DB with Geological data IP->GPS
        self.Geo            = {'default': {'long': 49.8, 'lat': 18.2 }}                                     # A placeholder for geoip DB in format: {'x.x.x.x':{'long': x.x, 'lat': x.x},...}
        self.possible_probe_codes = map(''.join, list(itertools.product('0123456789ABCDEF', repeat=3)))
        self.probe_codes    = {}
        self.Zapi.login(self.Cfg.zabbix_login, self.Cfg.zabbix_password)


    #Go through all probes and check if two probes hasn't same coordinates,
    # if yes it moves one probe to left or right (depends on which half of map probe lays), new position is checked again
    def check_position(self, x, y):
        mover = 60; #move pixels if two probes with same coordinates
        for ix in self.x_check:
            if (ix + mover) > x and (ix - mover) < x:
                if (self.y_check[ix] + mover ) > y and (self.y_check[ix] - mover) < y:
                    halfscreen = self.width / 2
                    if(x > ( halfscreen )):
                        if((x - mover) > halfscreen):
                            checked = self.check_position(x - mover, y)
                        else:
                            checked = self.check_position(x + mover, y)
                    else:
                        if((x + mover) > halfscreen):
                            checked = self.check_position(x + mover, y)
                        else:
                            checked = self.check_position(x - mover, y)
                    #IF it is OK, add it to checked array
                    self.x_check.append(checked['x'])
                    self.y_check[checked['x']] = y
                    return checked
        self.x_check.append(x)
        self.y_check[x] = y
        return {'x': x , 'y': y}


    # get list of nodes and their items, which will be shown on map. Items has in their names target Mac address,
    # so there can be made edge between two probes
    def get_edges(self):
        fields = ["host", "name", "hostid", "status"]               # fields = 'extend' for complete list
        self.probes = self.Zapi.host.get({ "output":fields,"filter": {'status':0}})
        #initial minimals and maximals for finding values coordinates that will be on the edge of map
        self.limits = {
            'maxLat' : 0,
            'minLat' : 360,
            'maxLon' : 0,
            'minLon' : 360
        }
        self.geodata = {}
        for p in self.probes:
            self.probe_codes[p['host']] = self.possible_probe_codes.pop(0)
            #assign coordinates to host
            if p['name'] in self.Geo.keys():
                self.geodata[p['host']] = {
                    'longitude': self.Geo[p['name']]['long'],
                    'latitude': self.Geo[p['name']]['lat']
                        }
            else:
                self.geodata[p['host']] = {
                    'longitude' : self.Geo['default']['long'],
                    'latitude'  : self.Geo['default']['lat']
                        }

            #geodata = self.Geo.record_by_addr(p['name'])
            geodata = self.geodata[p['host']]   #Get GPS coords for IP address
            if geodata != None:
                if geodata['latitude'] > self.limits['maxLat']:
                    self.limits['maxLat'] = geodata['latitude']
                if geodata['latitude'] < self.limits['minLat']:
                    self.limits['minLat'] = geodata['latitude']
                if geodata['longitude'] < self.limits['minLon']:
                    self.limits['minLon'] = geodata['longitude']
                if geodata['longitude'] > self.limits['maxLon']:
                    self.limits['maxLon'] = geodata['longitude']

            self.edges[p['host']] = {}
            #Get last values for all probe items
            items = self.Zapi.item.get({ "output":['name','lastvalue'],"filter": {'hostid':p['hostid']}})
            for item in items:
                parts = item['name'].split('-')

                if parts[0] == 'pesq' and len(parts) == 2 and parts[1] != '':
                    self.Grph.add_edge(p['host'], parts[1], value = item['lastvalue'])
                    self.Grph.node[p['host']]["hostname"] = p['name']
                    self.edges[p['host']][parts[1]] = item['lastvalue'] # {mac:{mac:pesq, mac:pesq....}, mac...}
                    print "Good edge "+p['host']+" "+parts[1]+" "+p['name']+" val "+item['lastvalue']
                else:
                    print "Bad edge "+p['host']+" "+p['name']+" "+str(parts)

    def count_multipliers(self): # Vypocet maxima a minima zobrazovane mapy
        denomX          = self.limits['maxLon'] - self.limits['minLon']
        if denomX == 0:
            denomX = 1
        denomY          = self.limits['maxLat'] - self.limits['minLat']
        if denomY == 0:
            denomY = 1
        self.multiplierX = ( self.width * self.padding) / denomX
        self.multiplierY = ( self.height * self.padding) / denomY

    def pair_labels(self):
        for e in self.edges:
            self.edges_labeled[e] = {}
            for e2 in self.edges[e]:
                if e2 != '':
                    if ( e in self.edges_labeled and e2 in self.edges_labeled[e] ) or ( e2 in self.edges_labeled and e in self.edges_labeled[e2]):
                        label = ''
                        print "Empty label "+e2+" "+e;
                    else:
                        #label = e+" > "+e2+":"+self.edges[e][e2]+" "
                        label = self.probe_codes[e] + ' : ' + self.edges[e][e2]+" "
                        #label += ", "+e2+" > "+e+":"+self.edges[e2][e]
                        if e2 in self.edges and e in self.edges[e2]:
                            label += "| " + self.probe_codes[e2] + ' : ' + self.edges[e2][e]
                        self.edges_labeled[e][e2] = label

    # prepare temporary graph beacause of coordinates of nodes, to place them in place where they will not collide
    def prepare_graph(self):
        self.pos = nx.graphviz_layout(self.Grph)
        self.poslist=list(self.pos.values())
        self.maxpos=map(max, zip(*self.poslist))

        for host, coordinates in self.pos.iteritems():
            self.pos[host] = [int(coordinates[0]*self.width/self.maxpos[0]*0.95-coordinates[0]*0.1), int((self.height-coordinates[1]*self.height/self.maxpos[1])*0.95+coordinates[1]*0.1)]
        nx.set_node_attributes(self.Grph,'coordinates',self.pos)
        selementids = dict(enumerate(self.Grph.nodes_iter(), start=1))
        selementids = dict((v,k) for k,v in selementids.iteritems())
        nx.set_node_attributes(self.Grph,'selementid',selementids)

    # FROM prepared graph generate zabbix map and insert all nodes and edges
    def create_map(self):
        map_params = {
            "name": self.map_name,
            "label_type": 0,
            "width": self.width,
            "height": self.height,
            "users": [
                {
                    "sysmapuserid": "6",
                    "userid": "2",
                    "permission": "2"
                }
            ],
            "userGroups": [
                {
                    "sysmapusrgrpid": "6",
                    "usrgrpid": "8",
                    "permission": "2"
                }
            ]
        }
        element_params=[]
        link_params=[]

        # check the current map for the coordinates of the probes using their label value
        #print "!!!!!!!!!!!!!!!!!"
        if len(self.Zapi.map.get({"output":"extend", "selectSelements": "extend", "filter":{"name":self.map_name}})) != 0:
            prev_selements = self.Zapi.map.get({"output":"extend", "selectSelements": "extend", "filter":{"name":self.map_name}})[0]['selements']
            working_sel_dict = {}
            for sel in prev_selements:
                ip, mac, loc    = map(lambda x: x.strip(), sel['label'].split('\n'))
                x               = sel['x']
                y               = sel['y']
                working_sel_dict[mac] = {'ip': ip, 'x': x, 'y': y}
        else:
            working_sel_dict = {}


        for node, data in self.Grph.nodes_iter(data=True):

            if node in self.geodata:
                pos_x = int((1 - self.padding) * self.width / 2) + self.padding * self.width  - int((self.limits['maxLon'] - self.geodata[node]['longitude'])* self.multiplierX )
                pos_y = int((self.limits['maxLat'] - self.geodata[node]['latitude']) * self.multiplierY )
                lat = self.geodata[node]['longitude']
                long = self.geodata[node]['latitude']
            #else:          # this block should not be necessary due to the default self.Geo setting
                #print '!!!!!!!!!!!!!!!!!!!!!!!! In'
                #pos_x = 1
                #pos_y = 1
                #lat = ''
                #long = ''
            if node in working_sel_dict.keys():         # if user changed probe's location, then use it as override
                positions = {
                        'x': working_sel_dict[node]['x'],
                        'y': working_sel_dict[node]['y']
                        }
            else:                                       # otherwise place it according to default rules
                positions = self.check_position(pos_x, pos_y)

            if 'hostname' in data:
                hostname = data['hostname']
            else:
                hostname = 'unknown'
            element_params.append({
                "elementtype": 4,
                "elementid": 0,
                "selementid": data['selementid'],
                "iconid_off": "74",
                "use_iconmap": 0,
                "label": hostname+' \n '+node+' \n '+ self.probe_codes[node] , #node,
                "x": positions['x'], #data['coordinates'][0],
                "y": positions['y'] #data['coordinates'][1],
                #"x": data['coordinates'][0],
                #"y": data['coordinates'][1]
            })


        # Iterate through edges to create the Zabbix links, based on selementids
        nodenum = nx.get_node_attributes(self.Grph,'selementid')

        for nodea, nodeb in self.Grph.edges_iter():
            if nodea in self.edges_labeled and nodeb in self.edges_labeled[nodea]:
                label = self.edges_labeled[nodea][nodeb]
            elif nodea in self.edges_labeled and nodea in self.edges_labeled[nodeb]:
                label = self.edges_labeled[nodeb][nodea]
            else:
                label = ""
                #print "Empty label " + nodea + " " +nodeb

            link_params.append({
                "selementid1": nodenum[nodea],
                "selementid2": nodenum[nodeb],
                "label": label
                })

        # Join the map data together
        map_params["selements"] = element_params
        map_params["links"] = link_params

        # Get rid of an existing map of that name and create a new one
        del_mapid = self.Zapi.map.get({"filter": {"name": self.map_name}})
        if del_mapid:
            self.Zapi.map.delete([del_mapid[0]['sysmapid']])

        zmap=self.Zapi.map.create(map_params)
        new_mapid = self.Zapi.map.get({"filter": {"name": self.map_name}})

        #Save id of new map into file for display on websites

        f = open(self.Cfg.map_id_counter,'w')
        f.write( new_mapid[0]['sysmapid']) # python will convert \n to os.linesep
        f.close()
        print "New map has ID "+new_mapid[0]['sysmapid']


