#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
import MySQLdb
from pprint import *

class Db(object):
    db = object
    def __init__(self, cfg):
        self.db = MySQLdb.connect( host=cfg.mysql_host, user=cfg.mysql_user, passwd=cfg.mysql_pass, db=cfg.mysql_db)

    def add_task(self, file, probe):
        cur = self.db.cursor() 
        cur.execute("INSERT INTO cron_tasks (id, file_name, from_probe, status, created, updated) VALUES (null, '"+file+"','"+probe+"',0, NOW(), '0000-00-00 00:00:00')");
        return cur.fetchone()

    def get_tasks(self, limit):
        cur = self.db.cursor() 
        cur.execute("SELECT * FROM cron_tasks WHERE status = 0 LIMIT "+str(limit)+" ");
        return cur.fetchall()

    def done(self, id, status):
        cur = self.db.cursor()
        cur.execute("UPDATE cron_tasks SET status = "+str(status)+", updated = NOW() WHERE id = "+str(id)+" ")

    def truncate(self):
        cur = self.db.cursor()
        cur.execute("TRUNCATE cron_tasks")





