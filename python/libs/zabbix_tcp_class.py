#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
from pprint import pprint
import socket
import struct
import json
import time
import logging
import subprocess

#Trida pro komunikaci mezi sondou a serverem pomocí TCP rozhraní
class ZabbixTcp(object):
    active_items = []       # pole do kterho je ulozen seznam aktivnich itemu 
    debug = False           # atribut ktery pokud je nastaven na True, tak zobrazuje vracena data pri vsech pozadavich na standardni vystup
    error = ''              # promena do ktere je ulozen text chyby pokud je pri pozadavku vracena

    def __init__(self):
        self.Cfg = Config()

    #Zaslani sledovanych dat ze sondy na server, Params: nazev itemu, hodnota itemu 
    def send_item(self, host, key, value, timeStamp):
        print host+"--"+key+"--"+value+"--"+timeStamp
        now = int(time.time())
        result = self.send(
                {"request":"agent data",
                 "data":[
                    {
                        "host": host,
                        "key": str(key),
                        "value": value,
                        "clock": str(timeStamp)
                    }
                 ],
                 "clock": str(now)
             })
        #logging.info( time.strftime("%Y-%m-%d %H:%M:%S") + ' send data - '+key+':'+str(value))
        return result
    
    #Ziskani seznamu aktivnich itemů ze serveru zabbix, tyto itemy server vyžaduje pro zasílání od sond, ulozeni do pole self.active_items    
    def get_active_items(self):
        result = self.send({"request":"active checks","host": Config.host_name})
        if(result == False):
            pprint(tcp.error)
            #logging.info( time.strftime("%Y-%m-%d %H:%M:%S") +' get active items - '+tcp.error)
        else:
            self.active_items = result['data']
            #logging.info( time.strftime("%Y-%m-%d %H:%M:%S") + ' get active items')
        return result
    
    #Zaslani pozadavku na Zabbix server, univerzalni metoda zasila data spolu s pozadovanou hlavickou skrze tcp port
    def send(self, data=None):
        print data
        #DATA =  str(data).replace("'","\"")
        #DATA = '{"data": [{"host": "000c29b3ce4c", "value": "4.516", "key": "pesq-000c2968612d", "clock": "20151110064414"}], "request": "agent data", "clock": "1447141500"}'
        #data = {"data": [{"host": "000c29b3ce4c", "value": "4.516", "key": "pesq-000c2968612d", "clock": "20151110064414"}], "request": "agent data", "clock": "1447141500"}
        try:
            p = subprocess.Popen(
                [
                    'zabbix_sender',
                    '-z',
                    self.Cfg.host,
                    '-s',
                    data['data'][0]['host'],
                    '-k',
                    data['data'][0]['key'],
                    '-o',
                    data['data'][0]['value']
                ],
                stdout = subprocess.PIPE,
                stderr = subprocess.PIPE
            )
            stdout, stderr = p.communicate()
            print stdout
            print stderr
        except Exception as e:
            print "Exception occured: %s" % e
            
        #zabbix_sender -z 127.0.0.1 -s "000c29b3ce4c" -k "pesq-000c2968612d" -o "4.516"
        #HEADER = '''ZBXD\1%s%s'''
        #if(self.debug == True):
        #    pprint(DATA);
        #data_length = len(DATA)
        #data_header = struct.pack('i', data_length) + '\0\0\0\0'
        #data_to_send = HEADER % (data_header, DATA)
        #sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#        sock.connect((Config.host, Config.port))
#        sock.send(data_to_send)
#        response_header = sock.recv(5)
#        
#        if not response_header == 'ZBXD\1':
#            self.error += 'wrong header returned'
#            return False
#
#        response_data_header = sock.recv(8)
#        response_data_header = response_data_header[:4] 
#        response_len = struct.unpack('i', response_data_header)[0]
#
#        response_raw = sock.recv(response_len)
#
#        sock.close()
#        try:
#            response = json.loads(response_raw)
#        except ValueError:
#            self.error += 'Json not returned'
#            return False
#        print '!!!!!!!!!!!!!!!!!!!!!!!'
#        print DATA
#        print response
#        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
#        return response

#zt=ZabbixTcp()
#zt.send()    
