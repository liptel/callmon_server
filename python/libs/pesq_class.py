#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config import Config
import sys
import getopt
import os.path
import time
import datetime
from pprint import *

class Pesq(object):
    def __init__(self):
        self.Cfg = Config()

    def parse_result(self, text):
        values = False
        for t in text.split('\n'):
            if 'P.862 Prediction (Raw MOS, MOS-LQO):' in t:
                parts = t.split('= ')
                values = parts[1].split('\t')
                break
            if 'error' in t:
                print t
                break
        return values
    
    def analyze(self, file):
        if os.path.isfile(Config.wav_path + file) == False:
            print 'File '+file+' not found'
            return False
        command = self.Cfg.qtest + ' +8000 '+ Config.reference_wav +' '+ Config.wav_path + file
        #print command
        return self.parse_result(os.popen(command).read())
    
    def get_target_mac(self, file):
        parts = file.split('-')
        try:
            ret = parts
        except IndexError:
            ret = False
        return ret;
    def to_timestamp(self, timeString):
        return int(datetime.datetime.strptime(timeString, '%Y%m%d%H%M%S').strftime("%s"))
    
